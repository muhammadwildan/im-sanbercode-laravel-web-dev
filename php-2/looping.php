<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Looping</title>
</head>
<body>
    <h1>Soal Looping</h1>
    <?php
    echo "<h3>Soal 1</h3>";

    echo "<h5>Looping Pertama</h5>";
    $x = 2;
    do {
        echo $x . " - I Love PHP <br>";
        $x +=2;
    }while($x <= 20);

    echo "<h5>Looping Kedua</h5>";
    $y = 20;
    while ($y >= 2) {
        echo $y . "- I Love PHP <br>";
        $y -= 2;
    }

    echo "<h3>Soal 2</h3>";

    $angka = [18, 45, 29, 61, 47, 34];

    echo "array Nomor : ";
    print_r($angka);

    foreach($angka as $value){
        $rest[] = $value%=5;
    }

    echo "<br>";
    echo "Hasil Array Modulus 5 : ";
    print_r($rest);

    echo "<h3>Soal 3</h3>";

    $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach($items as $val){
        $tampung = [
            "id" => $val[0],
            "name" => $val[1],
            "price" => $val[2],
            "description" => $val[3],
            "source" => $val[4],
        ];
        print_r($tampung);
        echo "<br>";
    }

    echo "<h3>Soal 4</h3>";

    for($i=1; $i<=5; $i++){
        for($j=1; $j<=$i; $j++){
            echo " * ";
        }

        echo "<br>";
    }

    ?>
</body>
</html>
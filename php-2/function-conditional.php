<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Sanbercode!"
*/

// Code function di sini

function greetings($nama){
    echo "Hai ". $nama . ", Selamat Datang! <br>";
}

greetings("Bagas");
greetings("Wahyu");
greetings("Wildan");

echo "<br>";

echo "<h3> Soal No 2 </h3>";

function reverse($kata1){
    $panjangKata = strlen($kata1);
    $tampung = "";
    for($i = $panjangKata = 1; $i>=0; $i--){
        $tampung .= $kata1[$i];
    }
    return $tampung;
}

function reverseString($kata2){
    $revString = reverse($kata2);
    echo $revString . '<br>';
}

reverseString("Wildan");
reverseString("Sanbercode");
reverseString("We Are Sanbers Developers");
echo "<br>"







?>

</body>

</html>
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use PhpParser\Node\Expr\Cast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'home']);

Route::get('/pendaftaran', [AuthController::class,'register']);

Route::post('kirim', [AuthController::class,'kirim']);

Route::get('/data-table', function(){
    return view ('page.data-table');
});

// Route::get('/table', function(){
//     return view ('page.table');
// });

Route::get('table', function(){
    return view('page.table');
});

//----------CRUD---------
//CREATE DATA

//Route Mengarah ke form data Cast
Route::get('/cast/create', [CastController::class, 'create']);

//Route Untuk Menyimpan Inputan Kedalam Database Cast
Route::post('/cast', [CastController::class, 'store']);

//READ DATA
//Route Mengarah ke Halaman Tampil Semuda Data di Table Cast
Route::get('/cast', [CastController::class, 'index']);


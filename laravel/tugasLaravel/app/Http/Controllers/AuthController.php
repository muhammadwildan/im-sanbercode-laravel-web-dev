<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.form');
    }

     public function kirim(Request $request)
    {
        //dd($request->all());
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('page.hello',['namaDepan' => $namaDepan ,'namaBelakang' => $namaBelakang]);
    }
}

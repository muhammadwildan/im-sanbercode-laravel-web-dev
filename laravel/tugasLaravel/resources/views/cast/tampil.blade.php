@extends('layout.master')

@section('title')
    Halaman Tampil Cast
@endsection

@section('sub-title')
    cast
@endsection


@section('content')

<a href="/cast/create" class="btn btn-danger btn-sm my-2">Tambah Cast</a>

    <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <a href="">Detail</a>
            </td>
        </tr>
    @empty
        <tr>
            <td>Data Cast Kosong</td>
        </tr>
    @endforelse
  </tbody>
</table>

@endsection
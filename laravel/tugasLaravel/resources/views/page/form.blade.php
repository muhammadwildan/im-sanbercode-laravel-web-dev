@extends('layout.master')

@section('title')
  Buat Account Baru!
@endsection

@section('sub-title')
  create account!  
@endsection

@section('content')
   <h2>Buat Account Baru!</h2>
    <form action="/kirim" method="post">
    @csrf
    <label>First name:</label> <br /><br />
      <input type="text" name="fname" />
      <br /><br />
      <label>Last name:</label> <br /><br />
      <input type="text" name="lname" />
      <br /><br />
      <label>Gender:</label> <br /><br />
      <input type="radio" name="gender" value="1" />Male <br />
      <input type="radio" name="gender" value="2" />Female <br />
      <input type="radio" name="gender" value="3" />Other <br />
      <br />
      <label>Nationality:</label><br /><br />
      <select name="nationality">
        <option value="">Indonesian</option>
        <option value="">Malaysian</option>
        <option value="">Thailand</option></select
      ><br /><br />
      <label>Language Spoken:</label><br /><br />
      <input type="checkbox" name="language" value="1" />Bahasa Indonesia <br />
      <input type="checkbox" name="language" value="2" />English <br />
      <input type="checkbox" name="language" value="3" />Other <br />
      <br />
      <label>Bio:</label><br /><br />
      <textarea cols="20" rows="5"></textarea>
      <br />

      <input type="submit" value="Sign Up" />
    </form>
  
@endsection


   
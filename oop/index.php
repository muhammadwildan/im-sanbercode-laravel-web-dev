<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
echo "name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->legs . "<br>";
echo "cold_blooded : " . $sheep->cold_blooded . "<br> <br>";

$kodok = new Frog("buduk");
echo "name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold_blooded : " . $kodok->cold_blooded . "<br>";
echo "jump : " . $kodok->jump() . "<br>";


$sungokong = new Ape("kera sakti");
echo "name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold_blooded : " . $sungokong->cold_blooded . "<br>";
echo "yell : " . $sungokong->yell() . "<br>";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Soal Array</title>
</head>
<body>
    <h1>Tugas Soal Array</h1>

    <?php
    $nama = ["Muhammad","Wildan","Nuril","Anom"];

    print_r($nama);

    echo "<br>";

    echo "Data Nama <br>";
    echo "Total Nama : " . count($nama) . "<br>";
    echo "<ol>";
    echo "<li>" . $nama[0] . "</li>";
    echo "<li>" . $nama[1] . "</li>";
    echo "<li>" . $nama[2] . "</li>";
    echo "<li>" . $nama[3] . "</li>";
    echo "</ol>";
    echo "<br>";

    $bioNama = [
        ["Muhammad", 23,"Laravel"],
        ["Wildan", 24,"Bootsrap"],
        ["Nuril", 25,"React JS"],
        ["Anom", 26,"Flutter"],
    ];

    echo "<pre>";
    print_r($bioNama);
    echo "</pre>";



    ?>

</body>
</html>
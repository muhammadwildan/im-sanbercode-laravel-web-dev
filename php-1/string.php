<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Soal String</title>
</head>
<body>
    <h1>Tugas Soal String</h1>
    
    <?php
    echo "<h3>Contoh Soal 1</h3>";

    $kalimat1 = "lorem ipsum";
    echo "Kalimat pertama : " . $kalimat1 . "<br>";
    echo "Panjang kalimat 1 : " . strlen($kalimat1) . "<br>";
    echo "Jumlah Kata Kalimat 1 : " . str_word_count($kalimat1) . "<br>";

    echo "<h3>Contoh Soal 2</h3>";

    $kalimat2 = "Selamat datang di sanbercode";
    echo "Kalimat Kedua " . $kalimat2 . "<br>";
    echo "Kata 1 Kalimat kedua : " . substr($kalimat2,0,7) . "<br>";
    echo "Kata 2 Kalimat kedua : " . substr($kalimat2,8,6) . "<br>";
    echo "Kata 3 Kalimat kedua : " . substr($kalimat2,15,2) . "<br>";
    echo "kata 4 Kalimat kedua : " . substr($kalimat2,18,10) . "<br>";


    echo "<h3>Contoh Soal 3</h3>";

    $kalimat3 = "Good Morning";
    echo "Kalimat ketiga " . $kalimat3 . "<br>";
    echo "Ganti string kalimat ke 3 : " . str_replace("Morning","Night",$kalimat3);

    ?>


</body>
</html>